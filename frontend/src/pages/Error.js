// Activity S53
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';

const Error = () => {
  return (
    <div>
      <h1>Page Not Found</h1>
      <p>The page you are looking for is not found.</p>
      <Link to="/">
        <Button variant="success">
          Back to Home
        </Button>
      </Link>
    </div>
  );
};

export default Error;