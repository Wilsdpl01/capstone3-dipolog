import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Card} from 'react-bootstrap';
import {Navigate, useNavigate, Link} from 'react-router-dom';

/*Images*/
import logo from '../assets/banner1.png';

//sweetalert2 is a simple and useful package for generating user allerts with ReactJS
import Swal2 from 'sweetalert2';

import UserContext from '../UserContext';

export default function Login() {

    const navigate = useNavigate();

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);


    //Allows us to consume the UserContext object and it's properties to use for user validation
    const { user, setUser } = useContext(UserContext);

    // const [user, setUser] = useState(localStorage.getItem('email'));

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
                    method: 'POST',
                    headers: {
                        'Content-Type' : 'application/json'
                    },
                    body: JSON.stringify({
                        email: email,
                        password: password
                    })
                })
        .then(response => response.json())
        .then(data=> {
            // console.log(data);
            // localStorage.getItem('token', data);

            //if statement to check whether the login is successful.
            if(data === false){
                /*alert('Login unsuccessful!');*/
                //in adding sweetlaert2 you have to use the fire method
                Swal2.fire({
                    title: "Login unuccessful!",
                    icon: 'error',
                    text: 'Check your login credentials and try again',
                    customClass: {
                      confirmButton: 'custom-ok-button-class', 
                    },
                })

            }else{
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                /*alert('Login successful!');*/

                Swal2.fire({
                    title: 'Login successful!',
                    icon: 'success',
                    text: 'Welcome to Zuitt!',
                    customClass: {
                      confirmButton: 'custom-ok-button-class', 
                    },
                })

                if (user.isAdmin) {
                    navigate('/');
                 } else {
                    navigate('/');
                }
            }

        })
    }

    const retrieveUserDetails = (token) => {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        })
        .then((data) => {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });
        })
        .catch((error) => {
          console.error('Error while fetching user details:', error);
          // Handle the error scenario and show error messages if needed
        });
    };


    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== '' && email.length<=15){
            setIsActive(false);
        }else{
            setIsActive(true);
        }

    }, [email, password]);

    return (
        user.id === null || user.id ===undefined
        ?
        <Row>
            <Col className = 'justify-content-center
                              col-6 
                              mx-auto 
                              pt-5 
                              d-flex' 
                 style={{ height: '100vh' }}>
                 
                <Card className="cardHighlight p-3" 
                      style={{ height: '370px' }}>
                    <div className="d-flex justify-content-center"> 
                        <img src={logo} 
                             alt="Succulents Logo" 
                             className="l-logo" /> 
                    </div>

                    <h1 className="text-center mt-2 rl-header">Login</h1>

                    <Form onSubmit={(e) => authenticate(e)}>
                        <Form.Group controlId="userEmail">
                            <Form.Label className="f-label">Email Address</Form.Label>
                            <Form.Control 
                                type="email" 
                                placeholder="Enter email"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="password">
                            <Form.Label className="f-label">Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <div className="text-center mt-2">
                            <p>Don't you have an account? <Link to="/register">Register</Link></p>

                            <Button variant="success" 
                                    type="submit" 
                                    id="submitBtn" 
                                    disabled = {isActive} 
                                    className ='mt-1 mb-1'>
                                     Login
                            </Button>  
                        </div>
                    </Form> 
                </Card>
            </Col>
        </Row>
        :
        <Navigate to = '/*' /> 
    )
}
