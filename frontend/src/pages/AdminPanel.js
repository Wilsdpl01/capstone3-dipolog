import { Fragment, useState, useEffect, useContext } from "react";
import { Table, Button, Modal, Form, Container } from "react-bootstrap";
import UserContext from "../UserContext";
import Swal2 from "sweetalert2";

// images
import createIcon from '../assets/create.png';
import dashIcon from '../assets/dashicon.png';

export default function AdminPanel() {
  // State hooks for the products, loading, error and modal
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [showModal, setShowModal] = useState(false);

  // State hooks for the product form inputs
  const [productName, setProductName] = useState("");
  const [productDescription, setProductDescription] = useState("");
  const [productPrice, setProductPrice] = useState(0);
  const [image, setImage] = useState("");

  // State hook for the selected product id
  const [selectedProductId, setSelectedProductId] = useState(null);

  // Context hook for the user data
  const { user } = useContext(UserContext);

  // Effect hook for fetching all products
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then((response) => response.json())
      .then((data) => {
        console.log(data)
        setProducts(data);
        setLoading(false);
      })
      .catch((error) => {
        setError(error.message);
        setLoading(false);
      });
  }, []);

  // Function for handling the modal show and hide
  const handleShowModal = () => setShowModal(true);
  const handleCloseModal = () => setShowModal(false);

  // Function for handling the product creation
  const handleCreateProduct = () => {
    // Create an object with the product form inputs
    const newProductData = {
      productName: productName,
      productDescription: productDescription,
      productPrice: productPrice,
      image: image,
    };

    // Send a post request to the backend create product route with the new product data and the user token
    fetch(`${process.env.REACT_APP_API_URL}/products/createProduct`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify(newProductData),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.error) {
          // If the product creation failed due to an error, show an error message
          Swal2.fire({
            title: "Product creation failed!",
            icon: "error",
            text: data.error,
            customClass: {
              confirmButton: 'custom-ok-button-class', 
            },
          });
        } else if (data.duplicate) {
          // If the product creation failed due to duplicate product name, show an error message
          Swal2.fire({
            title: "Product creation failed!",
            icon: "error",
            text: "A product with the same name already exists.",
            customClass: {
              confirmButton: 'custom-ok-button-class', 
            },
          });
        } else {
          // If the product creation is successful, show a success message and update the products state
          Swal2.fire({
            title: "Product created!",
            icon: "success",
            text: "You have successfully created a new product.",
            customClass: {
              confirmButton: 'custom-ok-button-class', 
            },
          });
          setProducts([...products, data]);
          handleCloseModal();
        }
      })
      .catch((error) => console.log(error));
  };

  // Function for handling the product update
  const handleUpdateProduct = () => {
    // Create an object with the product form inputs
    const updatedProductData = {
      productName: productName,
      productDescription: productDescription,
      productPrice: productPrice,
      image: image,
    };
    // Send a put request to the backend update product route with the updated product data and the user token
    fetch(
      `${process.env.REACT_APP_API_URL}/products/update/${selectedProductId}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify(updatedProductData),
      }
    )
      .then((response) => response.json())
      .then((data) => {
        // If the product update is successful, show a success message and update the products state
        if (data._id) {
          Swal2.fire({
            title: "Product updated!",
            icon: "success",
            text: "You have successfully updated the product information.",
            customClass: {
              confirmButton: 'custom-ok-button-class', 
            },
          });
          setProducts(
            products.map((product) =>
              product._id === selectedProductId ? data : product
            )
          );
          handleCloseModal();
        } else {
          // If the product update is not successful, show an error message
          Swal2.fire({
            title: "Product update failed!",
            icon: "error",
            text: data,
            customClass: {
              confirmButton: 'custom-ok-button-class', 
            },
          });
        }
      })
      .catch((error) => console.log(error));
  };

  // Function for handling the product activation or deactivation
  const handleToggleActive = (productId, isActive) => {
    // Create an object with the opposite value of isActive
    const toggleData = {
      isActive: !isActive,
    };
    // Send a patch request to the backend activate or archive product route with the toggle data and the user token
    fetch(
      `${process.env.REACT_APP_API_URL}/products/${
        isActive ? "archives" : "activate"
      }/${productId}`,
      {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify(toggleData),
      }
    )
      .then((response) => response.json())
      .then((data) => {
        // If the product activation or deactivation is successful, show a success message and update the products state
        if (data) {
          Swal2.fire({
            title: `Product ${isActive ? "deactivated" : "activated"}!`,
            icon: "success",
            text: `You have successfully ${
              isActive ? "deactivated" : "activated"
            } the product.`,
            customClass: {
              confirmButton: 'custom-ok-button-class', 
            },
          });
          setProducts(
            products.map((product) =>
              product._id === productId
                ? { ...product, isActive: !isActive }
                : product
            )
          );
        } else {
          // If the product activation or deactivation is not successful, show an error message
          Swal2.fire({
            title: `Product ${isActive ? "deactivation" : "activation"} failed!`,
            icon: "error",
            text: data,
            customClass: {
              confirmButton: 'custom-ok-button-class', 
            },
          });
        }
      })
      .catch((error) => console.log(error));
  };

  // Function for handling the modal form change
  const handleFormChange = (e) => {
    switch (e.target.name) {
      case "productName":
        setProductName(e.target.value);
        break;
      case "productDescription":
        setProductDescription(e.target.value);
        break;
      case "productPrice":
        setProductPrice(e.target.value);
        break;
      default:
        break;
    }
  };

  // Function for handling the modal form submit
  const handleFormSubmit = (e) => {
    e.preventDefault();
    if (selectedProductId) {
      handleUpdateProduct();
    } else {
      handleCreateProduct();
    }
  };

  // Function for handling the modal form reset
  const handleFormReset = () => {
    setProductName("");
    setProductDescription("");
    setProductPrice(0);
    setSelectedProductId(null);
  };

  // Function for handling the edit button click
  const handleEditClick = (productId) => {
    const productToEdit = products.find((product) => product._id === productId);
    setProductName(productToEdit.productName);
    setProductDescription(productToEdit.productDescription);
    setProductPrice(productToEdit.productPrice);
    setImage(productToEdit.image);
    setSelectedProductId(productId);
    handleShowModal();
  };

  // Function for handling the product deletion
  const handleDeleteProduct = (productId) => {
    // Send a delete request to the backend delete product route with the product ID and the user token
    fetch(`${process.env.REACT_APP_API_URL}/products/delete/${productId}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          Swal2.fire({
            title: "Product Deleted",
            text: "The product has been deleted successfully.",
            icon: "success",
            timer: 3000,
            timerProgressBar: true,
            customClass: {
              confirmButton: 'custom-ok-button-class', 
            },
          });
          // Remove the deleted product from the local state
          setProducts(products.filter((product) => product._id !== productId));
        } else {
          Swal2.fire({
            title: "Product Deletion Failed",
            text: data.error,
            icon: "error",
            customClass: {
              confirmButton: 'custom-ok-button-class', 
            },
          });
        }
      })
      .catch((error) => {
        Swal2.fire({
          title: "Error Occurred",
          text: "An error occurred while deleting the product.",
          icon: "error",
          customClass: {
              confirmButton: 'custom-ok-button-class', 
            },
        });
      });
  };

  // Function for rendering the table rows
  const renderTableRows = () => {
    // Sort the products array alphabetically based on productName
      const sortedProducts = products.slice().sort((a, b) => {
      const nameA = a.productName.toUpperCase();
      const nameB = b.productName.toUpperCase();
      return nameA.localeCompare(nameB);
    });

    return sortedProducts.map((product) => (
      <tr key={product._id}>
        <td>{product.productName}</td>
        <td>{product.productDescription}</td>
        <td>{`₱ ${product.productPrice}`}</td>
        <td>{product.isActive ? "Active" : "Inactive"}</td>
        <td className="text-center">
          <Button
            variant="success"
            onClick={() => handleToggleActive(product._id, product.isActive)}
            className="b-button mt-1"
          >
            {product.isActive ? (
              <p className="a-button">Disable</p>
            ) : (
              <p className="a-button">Enable</p>
            )}
          </Button>
          <Button variant="success" onClick={() => handleEditClick(product._id)} 
                  className="b-button mt-1"> 
            <p className="a-button">Edit</p>
          </Button>
          <Button variant="danger" onClick={() => handleDeleteProduct(product._id)} 
                  className="b-button mt-1 mb-1">
            <p className="a-button">Delete</p>
          </Button>
        </td>
      </tr>
    ));
  };

  if (!user.isAdmin) {
    return <div style={{ height: '100vh' }}></div>;
  }
    return (
      <Fragment>
        
        {products && (
          <Container>
            <div className = "text-center" 
                 style={{marginTop: '20px'}}>
              <img src={dashIcon} 
                   className="dashicon" />
            </div>
            <div>
              <Button variant="light" 
                      onClick={handleShowModal} 
                      className="c-button">
                <img src={createIcon} 
                     alt="Create Icon" 
                     className="d-button-icon" />
              </Button>
            </div>
            <Table className = "admin-info" 
                   striped bordered hover variant="success">
              <thead>
                <tr>
                  <th className="text-center">Name</th>
                  <th className="text-center">Product Description</th>
                  <th className="text-center">Price</th>
                  <th className="text-center">Status</th>
                  <th className="text-center">Actions</th>
                </tr>
              </thead>
              <tbody>{renderTableRows()}</tbody>
            </Table>

            <Modal show={showModal} 
                   onHide={handleCloseModal}>
              <Modal.Header closeButton>
                <Modal.Title>
                  {selectedProductId ? "Update Product" : "Create Product"}
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Form onSubmit={handleFormSubmit} 
                      onReset={handleFormReset}>
                  <Form.Group controlId="productName">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control
                      type="text"
                      name="productName"
                      value={productName}
                      onChange={handleFormChange}
                      required
                    />
                  </Form.Group>
                  <Form.Group controlId="productDescription">
                    <Form.Label>Product Description</Form.Label>
                    <Form.Control
                      as="textarea"
                      name="productDescription"
                      value={productDescription}
                      onChange={handleFormChange}
                      required
                    />
                  </Form.Group>
                  <Form.Group controlId="productPrice">
                    <Form.Label>Product Price</Form.Label>
                    <Form.Control
                      type="number"
                      name="productPrice"
                      value={productPrice}
                      onChange={handleFormChange}
                      required
                    />
                  </Form.Group>
                  <Form.Group controlId="productImage">
                    <Form.Label>Product Image URL</Form.Label>
                    <Form.Control
                      type="text"
                      name="productImage"
                      value={image}
                      onChange={(e) => setImage(e.target.value)}
                            required
                    />
                  </Form.Group>
                  <Modal.Footer>
                    <Button variant="secondary" type="reset">
                      <p className="e-button">Clear</p>
                    </Button>
                    <Button variant="success" type="submit">
                      <p className="e-button">{selectedProductId ? "Update" : "Create"}</p>
                    </Button>
                  </Modal.Footer>
                </Form>
              </Modal.Body>
            </Modal>
          </Container>
        )}
      </Fragment>
    );
   
}
