import { Fragment } from 'react';
import Banner from '../components/Banner';
import Footer from '../components/Footer';

export default function Home() {
  return (
    <Fragment>
      <Banner />
    </Fragment>
  );
}
