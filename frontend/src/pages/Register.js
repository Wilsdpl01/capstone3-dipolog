import {Button, Form, Row, Col, Card} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import { Navigate, useNavigate, Link } from 'react-router-dom'; 

/*Images*/
import logo from '../assets/banner1.png';

import UserContext from '../UserContext';
import Swal3 from 'sweetalert2';

export default function Register(){

    const navigate = useNavigate(); // Activity s54

    //State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const [isPassed, setIsPassed] = useState(true);

    const [isDisabled, setIsDisabled] = useState(true);

    //we are going to add/create a state that will declare whether the password1 and password 2 is equal
    const [isPasswordMatch, setIsPasswordMatch] = useState(true); 
    // Activity s54
    //Allows us to consume the UserContext object and it's properties to use for user validation
    const { user } = useContext(UserContext);

 
    //when the email changes it will have a side effect that will console its value
    useEffect(()=>{
        if(email.length > 15){
            setIsPassed(false);
        }else{
            setIsPassed(true);
        }
    }, [email]);

    // This useEffect will disable or enable the sign-up button
    useEffect(() => {
      // Check if all the required fields are filled and the passwords match
      if (
        email !== '' &&
        password1 !== '' &&
        password2 !== '' &&
        password1 === password2 &&
        email.length <= 20 &&
        firstName.length <= 15 &&
        lastName.length <= 15
      ) {
        setIsDisabled(false);
      } else {
        setIsDisabled(true);
      }
    }, [email, password1, password2, firstName, lastName]);

    
    //function to simulate user registration
    function registerUser(event) {
      event.preventDefault();

      fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          email: email
        })
      })
        .then((response) => response.json())
        .then((emailExists) => {
          if (emailExists) {
            Swal3.fire({
              title: 'User Exist',
              icon: 'error',
              text: 'Please use another email.',
              customClass: {
                confirmButton: 'custom-ok-button-class', 
              },
            });
          } else {
            fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
              method: 'POST',
              headers: { 'Content-Type': 'application/json' },
              body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password1
              })
            })
              .then((response) => response.json())
              .then((data) => {
                if (data.success) {
                  Swal3.fire({
                    title: 'Registration Successful',
                    icon: 'success',
                    text: 'Thank you for registering!',
                    customClass: {
                      confirmButton: 'custom-ok-button-class', 
                    },
                  }).then(() => {
                    navigate('/login');
                  });
                } else {
                  Swal3.fire({
                    title: 'Registration Failed',
                    icon: 'error',
                    text: 'Please try again.',
                    customClass: {
                      confirmButton: 'custom-ok-button-class', 
                    },
                  });
                }
              })
              .catch((error) => {
                console.error('Error during user registration:', error);
                Swal3.fire({
                  title: 'Registration Failed',
                  icon: 'error',
                  text: 'An error occurred during registration. Please try again.',
                  customClass: {
                    confirmButton: 'custom-ok-button-class', 
                  },
                });
              });
          }
        })
        .catch((error) => {
          console.error('Error checking email:', error);
          Swal3.fire({
            title: 'Error',
            icon: 'error',
            text: 'An error occurred while checking the email. Please try again.',
            customClass: {
              confirmButton: 'custom-ok-button-class', 
            },
          });
        });
    }

    //useEffect to validate whether the password1 is equal to password2
    useEffect(() => {
        if(password1 !== password2){
            setIsPasswordMatch(false);
        }else{
            setIsPasswordMatch(true);
        }

    }, [password1, password2]);

    return(
        user.id === null || user.id === undefined
        ?
          <Row>
            <Col className = "justify-content-center
                              col-6 
                              mx-auto 
                              pt-3 
                              pb-3 
                              d-flex">
              <Card className="cardHighlight 
                               p-3">
                <div className="d-flex 
                                justify-content-center"> 
                  <img src={logo} 
                       alt="Succulents Logo" 
                       className="l-logo"/> 
                </div>
                <h1 className = "text-center rl-header">Register</h1>
                <Form onSubmit ={event => registerUser(event)}>

                  <Form.Group 
                      className="mb-3" 
                      controlId="formBasicFirstName">
                    <Form.Label 
                      className="f-label">
                        First Name
                    </Form.Label>
                    <Form.Control 
                        type="firstName" 
                        placeholder="Enter firstname" 
                        value = {firstName}
                        onChange = {event => setFirstName(event.target.value)}/>
                    <Form.Text 
                        className="text-danger" 
                        hidden = {isPassed}>
                          The firstName should not exceed 15 characters!
                    </Form.Text>
                  </Form.Group>

                  <Form.Group 
                        className="mb-3" 
                        controlId="formBasicLastName">
                    <Form.Label 
                        className="f-label">
                         Last Name
                    </Form.Label>
                    <Form.Control 
                        type="lastName" 
                        placeholder="Enter lastname" 
                        value = {lastName}
                        onChange = {event => setLastName(event.target.value)}/>
                    <Form.Text 
                        className="text-danger" 
                        hidden = {isPassed}>
                          The lastname should not exceed 15 characters!
                    </Form.Text>
                  </Form.Group>

                  <Form.Group 
                        className="mb-3" 
                        controlId="formBasicEmail">
                    <Form.Label 
                        className="f-label">
                          Email Address
                    </Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email" 
                        value = {email}
                        onChange = {event => setEmail(event.target.value)}/>
                    <Form.Text 
                        className="text-danger" 
                        hidden = {isPassed}>
                          The email should not exceed 15 characters!
                    </Form.Text>
                  </Form.Group>

                  <Form.Group 
                        className="mb-3" 
                        controlId="formBasicPassword1">
                    <Form.Label 
                        className="f-label">
                          Password
                    </Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        value = {password1}
                        onChange = {event => setPassword1(event.target.value)}/>
                  </Form.Group>

                  <Form.Group 
                        className="mb-3" 
                        controlId="formBasicPassword2">
                    <Form.Label>Confirm Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Retype your nominated password" 
                        value = {password2}
                        onChange = {event => setPassword2(event.target.value)}/>

                    <Form.Text 
                        className="text-danger" 
                        hidden = {isPasswordMatch}>
                          The passwords does not match!
                    </Form.Text>
                  </Form.Group>

                  <div className="text-center mt-2">
                    <p>Have already an account? 
                       <Link to="/login">
                         Login
                       </Link>
                    </p>
                    <Button 
                        variant="success" 
                        type="submit" 
                        disabled = {isDisabled}>
                          Sign up
                    </Button>
                  </div>
                </Form>
              </Card>
            </Col>
          </Row>
        :
        <Navigate to = '*' />
    )
}

