import React, { Fragment, useState, useEffect } from 'react';
import { Row, Col, Form } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';
import Footer from '../components/Footer';

/*Images*/
import icon from '../assets/icon.png';

export default function Products() {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [searchTerm, setSearchTerm] = useState('');

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/products/active`);
        const data = await response.json();
        setProducts(data);
        setLoading(false);
      } catch (err) {
        setError(err.message);
        setLoading(false);
      }
    };

    fetchProducts();
  }, []);

  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
  };

  const filteredProducts = products.filter((product) => {
    return product.productName.toLowerCase().includes(searchTerm.toLowerCase());
  });

  return (
    <Fragment>
      {loading && <p style={{ height: "100vh" }}></p>}
      {error && <p>{error}</p>}

      <div>
        <Row>
          <Col xs={12} md={6} className="d-flex align-items-center">
            <img src={icon} alt="Search Icon" className="search-icon" />
            <Form className="mt-4">
              <Form.Control
                type="text"
                placeholder="Search Product"
                value={searchTerm}
                onChange={handleSearch}
                style={{ width: '200px' }}/>
            </Form>
          </Col>
        </Row>
        <Row className="mt-0">
          {filteredProducts
            .sort((a, b) => a.productName.localeCompare(b.productName))
            .map((product) => (
            <Col key={product._id} sm={12} md={9} lg={3} xl={3}>
              <ProductCard productProp={product} />
            </Col>
          ))}
        </Row>
      </div>  
    </Fragment>
  );
}
