import { Container, Row, Col, Button, Table, Form } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Swal from "sweetalert2";

/*Images*/
import shopIcon from '../assets/cartbanner.png';

const Cart = () => {
  // States
  const [cart, setCart] = useState([]);
  const [totalAmount, setTotalAmount] = useState(0);
  const [editedQuantities, setEditedQuantities] = useState({});
   const [checkoutData, setCheckoutData] = useState({
    productId: '',
    totalAmount: 0,
  });

  // Get token from local storage
  let token = localStorage.getItem('token');

  const refreshCart = () => {
    // Get the orders from the backend API
    fetch(`${process.env.REACT_APP_API_URL}/users/myOrders`, {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data); // Log the response data

        // Ensure that the API response is an array
        if (!Array.isArray(data)) {
          // Handle the case where the API response is not an array
          console.error('API response is not an array:', data);
          // Set cart and totalAmount to empty values or display an error message
          setCart([]);
          setTotalAmount(0);
          return;
        }

        // If the API response is an array, proceed with updating the cart state
        const orderedProducts = data.map((order) => ({
          _id: order._id,
          products: order.products,
          totalAmount: order.totalAmount,
          purchasedOn: order.purchasedOn,
        }));

        // Set the cart and totalAmount states based on the ordered products data
        setCart(orderedProducts);
        setTotalAmount(orderedProducts.reduce((acc, curr) => acc + curr.totalAmount, 0));
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    refreshCart();
  }, []);

  const deleteOrder = (objectId) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/delete/${objectId}`, {
      method: 'DELETE',
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data); // Log the response data
        refreshCart(); // Refresh the cart after deletion
        // Add the following line to refresh the page automatically
        window.location.reload();
      });
  };

  const editQuantity = (objectId, quantity) => {
    setEditedQuantities((prevState) => ({
      ...prevState,
      [objectId]: quantity
    }));

    fetch(`${process.env.REACT_APP_API_URL}/users/update/${objectId}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify({ quantity })
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data); // Log the response data
        refreshCart(); // Refresh the cart after editing the quantity
      })
      .catch((error) => {
        console.log(error);
        // Handle error scenario if needed
      });
  };

  
  // Calculate total amount based on edited quantities
    useEffect(() => {
        // Calculate total amount based on edited quantities
        const updatedTotalAmount = cart.reduce((acc, order) => {
          const orderTotal = order.products.reduce((total, product) => {
            const editedQuantity = editedQuantities[product._id];
            const quantity = editedQuantity !== undefined ? parseInt(editedQuantity) : product.quantity;
            return total + quantity * product.price;
          }, 0);
          return acc + orderTotal;
        }, 0);
        setCheckoutData((prevData) => ({ ...prevData, totalAmount: updatedTotalAmount }));
      }, [editedQuantities, cart]);

  const handleCheckout = () => {
    // Create an array of objects with productId and quantity
    const productsForCheckout = cart.flatMap((order) =>
      order.products.map((product) => ({
        productId: product.productId,
        quantity: editedQuantities[product._id] || product.quantity,
      }))
    );

    // Send the productsForCheckout to the backend API
    fetch(`${process.env.REACT_APP_API_URL}/checkout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        products: productsForCheckout,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data); // Log the response data
        // Use Swal alert to show the response message and data
        if (data.success) {
          // Checkout successful
          Swal.fire({
            icon: 'success',
            title: 'Checkout Successful',
            text: data.message,
            showConfirmButton: false,
            timer: 1500,
            customClass: {
              confirmButton: 'custom-ok-button-class', 
            },
          });
        } else {
          // Checkout failed
          Swal.fire({
            icon: 'error',
            title: 'Checkout Failed',
            text: data.message,
            showConfirmButton: false,
            timer: 1500,
            customClass: {
              confirmButton: 'custom-ok-button-class', 
            },
          });
        }
        // Refresh the cart after successful checkout
        refreshCart();
      })
      .catch((error) => {
        console.error('Error while processing checkout:', error);
        // Show an error alert using Swal
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'An error occurred while processing the checkout.',
          showConfirmButton: false,
          timer: 2000,
          customClass: {
              confirmButton: 'custom-ok-button-class', 
            },
        });
      });
  };


  return (
    <div className = "text-center">
      <Container className="pb-5">
        <Row>
          <Col lg={12} className="text-center">
            <img
              src={shopIcon}
              alt="Shop"
              className="nav-icon1"
              style={{
                marginTop: '30px',
                marginBottom: '5px',
                height: '90px',
                width: '300px'
              }}
            />
          </Col>
        </Row>
        {cart.length > 0 ? (
          <>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>ProductId</th>
                  <th>Quantity</th>
                  <th>Price</th>
                  <th>Total</th>
                  <th>Action</th> {/* Added the Action column */}
                </tr>
              </thead>
              <tbody>
                {cart.map((order) =>
                  order.products.map((product) => (
                    <tr key={product._id}>
                      <td>{product.productId}</td>
                      <td>
                        <Form.Control
                          type="number"
                          value={editedQuantities[product._id] || product.quantity}
                          onChange={(e) => editQuantity(order._id, e.target.value)}
                        />
                      </td>
                      <td>&#8369; {product.price}</td>
                      <td>&#8369; {product.totalAmount}</td>
                      <td>
                        <Button variant="danger" 
                                onClick={() => deleteOrder(order._id)} 
                                className="b-button mt-1 mb-1"
                        >
                          <p className="a-button">Delete</p>
                        </Button>
                      </td>
                    </tr>
                  ))
                )}
              </tbody>
            </Table>

            <h4 style={{ textAlign: 'left' }}>
                 Total Amount: &#8369; 
                 {totalAmount.toFixed(2)}
            </h4>
          </>
        ) : (
          <div className="py-3" style={{ height: '100vh' }}>
            <h2 className="mt-5">Your cart is empty</h2>
            <div className="my-3">
              <Button as={Link} to="/products" variant="success">
                 SHOP NOW!
              </Button>
            </div>
          </div>
        )}
      </Container>

      <Container>
        {cart.length > 0 && (
          <div className="py-3" style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Button variant="success" onClick={handleCheckout}>
              Checkout Now!
            </Button>
          </div>
        )}
      </Container>
    </div>
  );
};

export default Cart;