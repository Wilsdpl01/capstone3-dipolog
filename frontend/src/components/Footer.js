import React from 'react';

const Footer = () => {
  return (
    <footer className="text-center footer" 
            style={{ marginTop: '100px', 
                     padding: '20px' }}>

      {/* icons buttons Start*/}
      <div className="w-full max-w-screen-xl mx-auto px-4 pt-[4.5rem] pb-[2.5rem]">  
        <div className="p-1">
          <div className="icons mx-auto d-block">
            <a target="_blank" 
               rel="noopener noreferrer" 
               href="https://www.instagram.com/" 
               id="eight" 
               className="mr-3 icon--instagram" 
               style={{ textDecoration: 'none', 
                        marginRight: '10px' }}>
                <i className="ri-instagram-line" 
                  style={{ padding: '5px', 
                  borderRadius: '50%' }}>
                </i>
            </a>

            <a target="_blank" 
               rel="noopener noreferrer" 
               href="https://twitter.com/?lang=en" 
               id="eight" 
               className="mr-3 icon--twitter" 
               style={{ textDecoration: 'none', marginRight: '10px' }}>
                <i className="ri-twitter-line" 
                   style={{ padding: '5px', 
                            borderRadius: '50%' }}>
                </i>
            </a>

            <a target="_blank" 
               rel="noopener noreferrer" 
               href="https://www.facebook.com/" 
               id="eight" 
               className="mr-3 icon--fb" 
               style={{ textDecoration: 'none', 
                        marginRight: '10px' }}>
                <i className="ri-facebook-box-line" 
                   style={{ padding: '5px', 
                   borderRadius: '50%' }}>
                </i>
            </a>
              
            <a target="_blank" 
               rel="noopener noreferrer" 
               href="https://www.gmail.com/" 
               id="eight" 
               className="mr-3 icon--gmail" 
               style={{ textDecoration: 'none', 
                        marginRight: '10px' }}>
                <i className="ri-mail-line" 
                   style={{ padding: '5px', 
                   borderRadius: '50%' }}>
                </i>
            </a>
          </div>
        </div>
        {/* icons buttons End*/}
          {/* Creates an inline element */}
          <span className="block 
                           text-center 
                           text-sm 
                           text-gray-500 
                           sm:text-center 
                           dark:text-gray-400">
                           © 2023 Succulents Box. All Rights Reserved.
          </span>
      </div>
    </footer>
  );
};

export default Footer;