import { Card, Button, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({ productProp }) {
  
  const { _id, productName, productPrice, image } = productProp;

  return (
     <Row className="mt-0 mb-3">
        <Col className="d-flex justify-content-center">
          <Card className="mt-3" 
                bg="light" 
                style={{ width: '300px'}}>
            <Card.Img className="p-2" 
                      variant="top" 
                      src={image} 
                      alt={productName} 
                      style={{ width: '100%', 
                               height: '160px', 
                               borderRadius: '20px'}} />
            <Card.Body>
                <Card.Title>{productName}</Card.Title>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text style={{ fontSize: '15px', 
                                    marginTop: '5px' }}>
                            {`₱ ${productPrice}`}
                </Card.Text>
            </Card.Body>
              
            <Card.Footer>
                <Button as={Link} 
                        to={`/products/${_id}`} 
                        variant="success" 
                        className="w-100">
                         View Details
                </Button>
            </Card.Footer>
          </Card>
        </Col>
     </Row>
  );
}
