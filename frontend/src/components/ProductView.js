import { Row, Col, Button, Card, Form } from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';
import { useEffect, useState, useContext } from 'react';

import UserContext from '../UserContext';
import CartCard from './CartCard';

import Swal2 from 'sweetalert2';

export default function ProductView() {
  const [product, setProduct] = useState(null);
  const [quantity, setQuantity] = useState(1);
  const { id } = useParams();
  const navigate = useNavigate();
  const { user } = useContext(UserContext);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Product not found'); // Handle 404 error
        }
        return response.json();
      })
      .then((data) => {
        setProduct(data);
      })
      .catch((error) => {
        console.log(error);
        // You can display an error message to the user here if needed
      });
  }, [id]);

  const handleAddToCart = () => {
    if (user.id && !user.isAdmin) {
      const orderData = {
        productId: product._id,
        quantity: quantity,
        productName: product.productName,
        price: product.productPrice,
      };

      try {
        // Make a POST request to add the product to the cart
        // Replace the endpoint and headers with your actual API route and headers
        fetch(`${process.env.REACT_APP_API_URL}/users/add-to-cart`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
          body: JSON.stringify(orderData),
        })
          .then((response) => response.json())
          .then((data) => {
            // If the order is successful, show a success message and navigate to the cart page
            Swal2.fire({
              title: 'Product added to cart!',
              icon: 'success',
              text: 'The product has been added to your cart.',
              customClass: {
                  confirmButton: 'custom-ok-button-class', 
              },
            });
            navigate('/cart');
          })
          .catch((error) => {
            console.log(error);
            Swal2.fire({
              title: 'Failed to add product to cart!',
              icon: 'error',
              text: 'An error occurred while adding the product to your cart.',
              customClass: {
                  confirmButton: 'custom-ok-button-class', 
              },
            });
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      // If the user is not logged in, show a warning message and ask them to log in or register first
      // and ask them to log in or inform them that admins cannot add to cart
           if (!user.id) {
             Swal2.fire({
               title: 'Please log in or register first',
               icon: 'warning',
               text: 'You need to have an account to add products to your cart.',
               customClass: {
                  confirmButton: 'custom-ok-button-class', 
              },
             });
             navigate('/login');
           } else {
             Swal2.fire({
               title: 'Admins cannot add products to cart',
               icon: 'warning',
               text: 'You are logged in as an admin. Admins cannot add products to the cart.',
               customClass: {
                  confirmButton: 'custom-ok-button-class', 
               },
             });
           }
        }
    };

  if (!product) {
    return <div style={{ height: '100vh' }}></div>;
  }

  const totalAmount = quantity * product.productPrice;

  return (
    <Row className="mt-5 mb-3">
    
      <Col>
        <Card className="mx-auto" 
              style={{ width: '20rem' }} 
              bg="light">
          <Card.Img
              className="p-2"
              variant="top"
              src={product.image}
              alt={product.productName}
              style={{ width: '100%', 
                       height: '200px', 
                       borderRadius: '20px'}}/>
            <Card.Body>
              <Card.Title>{product.productName}</Card.Title>
              <Card.Text>{product.productDescription}</Card.Text>
              <Card.Text>
                 Price: 
                 <span>&#8369;</span> 
                 {product.productPrice}
              </Card.Text>
              <Form>
                <Form.Group controlId="quantity">
                  <Form.Label>Quantity</Form.Label>
                  <div className="input-group">
                    <Button
                      variant="outline-secondary"
                      id="btn-minus"
                      onClick={() => setQuantity(quantity - 1)}
                      disabled={quantity === 1}
                    >
                      -
                    </Button>
                    <Form.Control
                      type="number"
                      min={1}
                      value={quantity}
                      onChange={(e) => setQuantity(parseInt(e.target.value))}
                      className="text-center"
                    />
                    <Button
                      variant="outline-secondary"
                      id="btn-plus"
                      onClick={() => setQuantity(quantity + 1)}
                    >
                      +
                    </Button>
                  </div>
                </Form.Group>  
              </Form>
              
              <CartCard totalAmount={totalAmount} />

              <Card.Footer>
                <Button variant="success" 
                        onClick={handleAddToCart} 
                        className="w-100">
                         Add to Cart
                </Button>
              </Card.Footer>  
            </Card.Body>
        </Card>
      </Col>
    </Row> 
  );
}
