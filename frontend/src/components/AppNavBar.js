import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

/*Images*/
import logo from '../assets/logo1.png';
import shopIcon from '../assets/cart.png';

import {useState, useEffect, useContext} from 'react';
import {NavLink, Link} from 'react-router-dom';

import UserContext from '../UserContext';

// The as keyword allows components to be treated as if they are different component gaining access to it's properties and functionalities
//The to keyword is used in place of the 'href' for providing the URL for the page
export default function AppNavBar() {

    const { user } = useContext(UserContext);

    return (
        <Navbar expand="lg" 
                className='shadow'
                style={{backgroundColor: '#214032'}}
            >
            <Container fluid>
                <Navbar.Brand as={Link} to="/">
                    <img src={logo} 
                         alt="Succulents Logo" 
                         className="logo"/>
                </Navbar.Brand>

                <Navbar.Toggle aria-controls="basic-navbar-nav" 
                               style={{ backgroundColor: '#71B98B' }}/>

                <Navbar.Collapse id="basic-navbar-nav" 
                                 className = "text-center">
                    <Nav className="ms-auto">
                        <Nav.Link as = {NavLink} 
                                  to = '/'  
                                  style={{color: '#FDFDFB', marginTop: '5px'}} >
                                   HOME
                        </Nav.Link>

                        <Nav.Link as = {NavLink} to = '/products' 
                                  style={{color: '#FDFDFB', marginTop: '5px'}}>
                                   SHOP
                        </Nav.Link>

                        <Nav.Link as={NavLink} 
                                  to="/cart">
                            <img src={shopIcon} 
                                 alt="Shop" 
                                 className="nav-icon1"/>
                        </Nav.Link>
                        
                        {user.id === null || user.id === undefined ? (
                            <>
                                <Nav.Link as={NavLink} 
                                          to="/register" 
                                          style={{color: '#FDFDFB', marginTop: '5px'}}>
                                           Register
                                </Nav.Link>

                                <Nav.Link as={NavLink} 
                                          to="/login" 
                                          style={{color: '#FDFDFB', marginTop: '5px'}}>
                                           Login
                                </Nav.Link>
                            </>
                        ) : (
                            <>
                        {user.isAdmin && (
                                <Nav.Link className="page-header1" 
                                          as={NavLink} 
                                          to="/admin" 
                                          style={{color: '#FDFDFB', marginTop: '5px'}}>
                                           Dashboard
                                </Nav.Link>
                        )}
                                <Nav.Link className="page-header1" 
                                          as={NavLink} 
                                          to="/logout" 
                                          style={{color: '#FDFDFB', marginTop: '5px'}}>
                                           Logout
                                </Nav.Link>
                            </>
                        )}
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}
