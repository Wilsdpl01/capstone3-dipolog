import React, { useState } from "react";
import Cart from "./Cart";

function ParentComponent() {
  // Assuming you have the userId stored in state
  const [userId, setUserId] = useState("123456");

  return (
    <div>
      {/* Other components */}
      <Cart userId={userId} />
    </div>
  );
}

export default ParentComponent;