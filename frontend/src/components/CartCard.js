import React from 'react';

const CartCard = ({ totalAmount, productName }) => {
  return (
    <div className="cart-card">
      <h6>Total Amount: {totalAmount}</h6>
      <p> {productName} </p>
    </div>
  );
};

export default CartCard;
