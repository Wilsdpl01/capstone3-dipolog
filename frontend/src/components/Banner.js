import { Button, Row } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { motion } from 'framer-motion';
import { useRef, useEffect, useState } from "react";

/* Images */
import banner from '../assets/banner1.png';
import arrowIcon from '../assets/icon1.png';
import images from '../images';

export default function Banner() {

  const [width, setWidth] = useState(0);
  const carousel = useRef();

  useEffect(() => {
    setWidth(carousel.current.scrollWidth - carousel.current.offsetWidth);
  }, [])

  return (
    <div>
      <Container style={{ height: '70%' }}>
        <Row className="mt-3 d-flex justify-content-center">
          <div style={{ paddingRight: '120px', 
                        paddingLeft: '120px' }}>
            <p className="banner-text mt-5 text-center">
                Shop eco-friendly succulents for a greener world!
            </p>
            <p className="tag-line text-center">
                Join our mission for sustainable living.
            </p>
          </div>
          <Button
            as={Link}
            to="/products"
            className="shop-button"
            style={{
              background: "none",
              border: "none",
              color: "#214032",
              fontWeight: "bold",
              fontFamily: "Belanosima, sans-serif",
              fontSize: "20px",
              width: "170px",
              height: "45px"
            }}
          >
            Shop Now! <i className="fas fa-arrow-center"></i>
            <img
              src={arrowIcon}
              className="icon1"
              alt="Arrow Icon"
            />
          </Button>
        </Row>
        <Row className="App justify-content-center">
          <div className="App" 
               style={{ width: '90%', 
                        position: 'relative', 
                        height: '320px', 
                        overflow: 'hidden', 
                        marginTop: '20px' }}>
            <motion.div
              ref={carousel}
              className="carousel"
              whileTap={{ cursor: "grabbing" }}
            >
              <motion.div
                drag="x"
                dragConstraints={{ right: 0, left: -width }}
                className="inner-carousel"
              >
                {images.map(image => {
                  return (
                    <motion.div className="item" key={image}>
                      <img src={image} alt="" />
                    </motion.div>
                  );
                })}
              </motion.div>
            </motion.div>
          </div>
        </Row>
      </Container>
    </div>
  );
}

